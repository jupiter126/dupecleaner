#!/bin/bash
### Stupid script meant for one time recursive cleaning of duplicates of a specific extension in the running folder: all in ram no save state.
# 1. build a quick array index
# 2. Compare the checksum of each element of the array against all the other ones
# 3. If one has md5 dupes, sha256 is checked to avoid collisions then if dupe twice, file is deleted and removed from array.

######## Var Declaration ################
# The extension you want to look for dupes for:
eextension=txt
vverbal="1"  # Indicates progress
ttestmode="1" # Set to 0 to enable deletion
######## Init
recid=0
iindex=()
def=$(tput sgr0);bol=$(tput bold);red=$(tput setaf 1;tput bold);gre=$(tput setaf 2;tput bold);yel=$(tput setaf 3;tput bold);blu=$(tput setaf 4;tput bold);mag=$(tput setaf 5;tput bold);cya=$(tput setaf 6;tput bold) #colors
######## Code  ############################################################
######## Build Array
# quickly build a recursive index array : it has 3 fields: "recid:md5:filename"
# recid matches array index
for ffile in $(find ~+ -type f -name "*.$eextension"); do
	iindex+=("$recid:$(md5sum "$ffile"|sed "s/  /:/")")
	((recid=recid+1))
done
######## Parse Array and clean
for rrecords in "${iindex[@]}"; do
	ffilerecid="$(echo "$rrecords"|cut -f1 -d":")"
	ffilechecksum="$(echo "$rrecords"|cut -f2 -d":")"
	ffilename="$(echo "$rrecords"|cut -f3 -d":")"
	if [[ -f $ffilename ]]; then
		if [[ "$vverbal" = "1" ]]; then echo "$gre $ffilename: Checking for dupes$def"; fi
		for rrecords2 in "${iindex[@]}"; do
			ffilerecid2="$(echo "$rrecords2"|cut -f1 -d":")"
			ffilechecksum2="$(echo "$rrecords2"|cut -f2 -d":")"
			ffilename2="$(echo "$rrecords2"|cut -f3 -d":")"
			if [[ "$ffilename" != "$ffilename2" ]]; then
				if [[ "$ffilechecksum" = "$ffilechecksum2" ]]; then
					if [[ $(sha256sum $ffilename|cut -f1 -d" ") = $(sha256sum $ffilename2|cut -f1 -d" ") ]]; then
						if [[ "$vverbal" = "1" ]]; then echo "$red  - $ffilename2 is a dupe of $ffilename - removing$def"; fi
							unset 'iindex[$ffilerecid2]'
							if [[ "$ttestmode" = "0" ]]; then rm "$ffilename2"; fi
					else
						echo "$mag !!!  Houston we have an MD5 collision between $ffilename and $ffilename2  !!!$def"
					fi
				fi
			fi
		done
	fi
done
